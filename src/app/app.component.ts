import { Component } from '@angular/core';
import tasksData from '../assets/test.json';

interface Task {  
  scriptName: string;
  startDate: string;
  endDate: string;  
  result: Number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'tasksData';
  searchText = '';
  tasks: Task[] = tasksData.collection;
}